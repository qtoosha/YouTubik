/**
 * Created by kirk on 18-9-14.
 */

$(document).ready(function () {
    "use strict";

    $(".yt-search").on("click.yt-search", "button.cmd-search", function () {
        var resultsBlock = $(".yt-results");
        resultsBlock.fadeOut();
        resultsBlock.html("");
        $.get(
            "/yt",
            {
                url: $("input.in-ytid").val()
            },
            function (html) {
                resultsBlock.html(html);
                resultsBlock.fadeIn();
            },
            "html"
        );
    });
});
