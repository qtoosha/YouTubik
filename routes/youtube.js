var express = require('express');
var moment = require('moment');
var request = require("request");
//var yt = require("youtube-api");

var router = express.Router();

/* GET users listing. */
router.get('/', function (req, res) {
    try {
        var url = req.query.url;
        var results = url.match("[\\?&]v=([^&#]*)");
        var vid = ( results === null ) ? url : results[1];

        request.get(
            {
                url: "https://www.googleapis.com/youtube/v3/videos",
                qs: {
                    part: "snippet,contentDetails",
                    id: vid,
                    key: ""
                }
            },
            function (e, r, body) {
                var data = JSON.parse(body);
                console.log(data);

                if (data.items.length == 0) {
                    throw {
                        message: "No items found",
                        error: {}
                    }
                }

                var item = data.items[0];
                var duration = moment.duration(item.contentDetails.duration);
                var durationFormat = (duration.get("hours") > 0 ? "H:mm:ss" : "m:ss");

                res.render(
                    "thumbnail",
                    {
                        vurl: "https://www.youtube.com/watch?v=" + item.id,
                        url: item.snippet.thumbnails.default.url,
                        width: item.snippet.thumbnails.default.width,
                        height: item.snippet.thumbnails.default.height,
                        duration: moment.utc(duration.as("milliseconds")).format(durationFormat),
                        title: item.snippet.title
                    }
                );
            }
        );
    } catch (ex) {
        res.render(
            "error",
            {
                message: ex.message,
                error: ex
            });
    }
});

module.exports = router;
